<?php

namespace app\services;


use app\models\TicketSubtype;
use app\repository\TicketSubtypeRepository;

class TicketSubtypeService
{
    public static function getTicketSubtypeList()
    {
        $list = [];
        $repo = new TicketSubtypeRepository();
        $model = $repo->getColumn([]);
        foreach ($model as $value) {
            $list[$value['id']] = $value['name'];
        }
        return $list;
    }

    public static function getParentNameList()
    {
        $list = (new TicketSubtypeRepository())->getColumn('id,name,parent_id');
        $types = [];
        $parentType = TicketTypeService::getTypeList();
        foreach ($list as $type) {
            $parentName = $parentType[$type->parent_id];
            $types[$parentName][$type->id] = $type->name;
        }
        return $types;
    }
}
