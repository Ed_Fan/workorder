<?php

namespace app\services;

use app\models\Ticket;
use app\repository\MemberRepository;
use app\repository\TicketRepository;

class TicketService
{
    public static function changeStatus(Ticket $ticket, $operate)
    {
        $repository = new TicketRepository($ticket);
        if ($operate === 'close') {
            return $repository->closeTicket();
        } elseif ($operate === 'open') {
            return $repository->openTicket();
        } else {
            return false;
        }
    }
}