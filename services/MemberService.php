<?php

namespace app\services;

use app\models\Member;

class MemberService
{
    public static function getUserIdentity(Member $member)
    {
        $type = isset($member->type) ? (int)$member->type : null;
        $userIdentity = isset($member->getStateLabels()[$type]) ? $member->getStateLabels()[$type] : null;
        if ($userIdentity === '客服') {
            if (in_array($member->id, \Yii::$app->params['allowAdmin'])) {
                $userIdentity = 'super';
            }
        }
        if ($userIdentity) {
            \Yii::$app->session->set('userIdentity', $userIdentity);
        } else {
            return false;
        }
        return true;
    }
}
