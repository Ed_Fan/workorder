<?php

namespace app\services;

use app\common\helpers\UserIdentityHelper;

class MenuItemService
{
    public static function getMenuItems()
    {
        if (UserIdentityHelper::isAsker()) {
            $menuItems[] = [
                'label' => '提交工单',
                'url'   => ['ticket/create'],
            ];
            $menuItems[] = [
                'label' => '我的工单',
                'url'   => ['ticket/index'],
            ];
        } elseif (UserIdentityHelper::isAnswer()) {
            $menuItems[] = [
                'label' => '我的工单',
                'url'   => ['ticket/index'],
            ];
            $menuItems[] = [
                'label' => '系統設置',
                'items' => [
                    ['label' => '平台分類', 'url' => ['platform/index']],
                    ['label' => '問題分類', 'url' => ['ticket-type/index']],
                ],
            ];
            $menuItems[] = [
                'label' => '用戶管理',
                'url'   => ['member/index'],
            ];
            $menuItems[] = [
                'label' => '快速回復',
                'url'   => ['ticket/index'],
            ];

        }

        return isset($menuItems) ? $menuItems : [];
    }

}