<?php


namespace app\services;

use app\repository\PlatformRepository;

class PlatformService
{
    public static function getPlatformList()
    {
        $list = [];
        $repo = new PlatformRepository();
        $model = $repo->getColumn();
        foreach ($model as $value) {
            $list[$value['id']] = $value['name'];
        }
        return $list;
    }
}
