<?php

namespace app\services;

use app\repository\TicketTypeRepository;

class TicketTypeService
{
    public static function getTypeList()
    {
        $list = (new TicketTypeRepository())->getColumn('id,name');
        $types=[];
        foreach ($list as $type){
            $types[$type->id] = $type->name;
        }
        return $types;
    }

}