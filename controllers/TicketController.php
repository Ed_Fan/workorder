<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use app\models\Ticket;
use app\models\TicketSearch;
use app\services\TicketService;
use app\services\TicketSubtypeService;
use app\controllers\AuthController;
use app\common\helpers\UserIdentityHelper;
use app\common\helpers\FlashHelper;
use app\repository\PlatformRepository;
use app\repository\TicketRepository;


/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends AuthController
{

    /**
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TicketSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);
        if (UserIdentityHelper::isAsker()) {
            $dataProvider->query->andWhere(['create_by' => Yii::$app->user->id]);
        }
        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ticket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ticket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $repository = new TicketRepository();
        if ($repository->_model->load(Yii::$app->request->post())) {
            try {
                if ($repository->create()) {
                    return $this->redirect(['index']);
                }
            } catch (\ErrorException $e) {
                FlashHelper::error($e->getMessage());
                return $this->refresh();
            }
        }

        return $this->render('create', [
            'model' => $repository->_model,
        ]);
    }

    /**
     * Updates an existing Ticket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//        if ($model->load(Yii::$app->request->post())) {
//            try {
//                if (!$model->save()) {
//                    throw new \ErrorException(array_pop($model->getFirstErrors()));
//                }
//                return $this->redirect(['view', 'id' => $model->id]);
//            } catch (\ErrorException $e) {
//                FlashHelper::error($e->getMessage());
//                return $this->refresh();
//            }
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing Ticket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return bool|Response
     */
    public function actionCloseTicket($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        try {
            if (TicketService::changeStatus($model, 'close')) {
                return true;
            }
        } catch (\ErrorException $e) {
            FlashHelper::error($e->getMessage());
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @param $id
     * @return bool|Response
     */
    public function actionOpenTicket($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        try {
            if (TicketService::changeStatus($model, 'open')) {
                return true;
            }
        } catch (\ErrorException $e) {
            FlashHelper::error($e->getMessage());
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
