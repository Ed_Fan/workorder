<?php

namespace app\controllers;

use app\common\helpers\FlashHelper;
use Yii;
use app\models\TicketType;
use app\models\TicketTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketTypeController implements the CRUD actions for TicketType model.
 */
class TicketTypeController extends AuthController
{

    /**
     * Lists all TicketType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TicketTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TicketType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TicketType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TicketType();

        if ($model->load(Yii::$app->request->post())) {
            try {
                if (!$model->save()) {
                    throw new \ErrorException(array_pop($model->getFirstErrors()));
                }
                return $this->redirect(['index']);
            } catch (\ErrorException $e) {
                FlashHelper::error($e->getMessage());
                return $this->refresh();
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TicketType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            try {
                if (!$model->save()) {
                    throw new \ErrorException(array_pop($model->getFirstErrors()));
                }
                return $this->redirect(['index']);
            } catch (\ErrorException $e) {
                FlashHelper::error($e->getMessage());
                return $this->refresh();
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TicketType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TicketType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TicketType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TicketType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
