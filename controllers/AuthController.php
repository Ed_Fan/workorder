<?php
/**
 * Date: 17/3/27 21:43
 */

namespace app\controllers;

use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;

class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
//                        'actions' => ['*'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }

    public function beforeAction($action)
    {
        if (\Yii::$app->session->get('userIdentity')) {
            return true;
        }
        throw new HttpException(403, '您沒有權限訪問');
    }

    protected function onlyAsker()
    {
        if (\Yii::$app->session->get('userIdentity') === '用戶') {
            return true;
        }
        throw new HttpException(403, '您沒有權限訪問');
    }

    protected function onlyAnswer()
    {
        if (\Yii::$app->session->get('userIdentity') === '客服') {
            return true;
        }
        throw new HttpException(403, '您沒有權限訪問');
    }

    protected function onlySuper()
    {
        if (\Yii::$app->session->get('userIdentity') === 'super') {
            return true;
        }
        throw new HttpException(403, '您沒有權限訪問');
    }
}