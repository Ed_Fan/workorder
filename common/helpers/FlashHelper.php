<?php
/**
 * Created by PhpStorm.
 * User: obama
 * Date: 15/10/6
 * Time: 00:09
 */


namespace app\common\helpers;

class FlashHelper {

    public static function success($message)
    {
        \Yii::$app->session->setFlash('success',self::getFirstError($message));
    }
    public static function error($message)
    {
        \Yii::$app->session->setFlash('error',self::getFirstError($message));
    }
    public static function warning($message)
    {
        \Yii::$app->session->setFlash('warning',self::getFirstError($message));
    }
    public static function notice($message)
    {
        \Yii::$app->session->setFlash('notice',self::getFirstError($message));
    }

    public static function getFirstError($messages)
    {
        if( is_array($messages)) {
            $message = '';
            foreach( $messages as $val) {
                if( is_array($val)) {
                    $message = array_shift($val);
                    break;
                }
                $message = $val;
                break;
            }
            return $message;
        }
        return $messages;
    }
}
