<?php

/** * Date: 2017/6/26
 * Time: 上午 11:08
 */

namespace app\common\helpers;

class UserIdentityHelper
{
    public static function isAsker()
    {
        if (\Yii::$app->session->get('userIdentity') === '用戶') {
            return true;
        } else {
            return false;
        }
    }

    public static function isAnswer()
    {
        if (\Yii::$app->session->get('userIdentity') === '客服') {
            return true;
        }else {
            return false;
        }
    }

    public static function isSuper()
    {
        if (\Yii::$app->session->get('userIdentity') === 'super') {
            return true;
        }else {
            return false;
        }
    }
}