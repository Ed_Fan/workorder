<?php

namespace app\models;

use Yii;
use yii\base\ErrorException;

/**
 * This is the model class for table "ticket_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $template
 */
class TicketType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id'], 'required'],
            [['template'], 'string'],
            [['id'], 'number'],
            [['name'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'name'     => '問題分類',
            'template' => '模板',
        ];
    }

    /**
     * @inheritdoc
     * @return TicketTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TicketTypeQuery(get_called_class());
    }

    /**
     * @return bool
     * @throws ErrorException
     */
    public function beforeSave()
    {
        if ($this->isAttributeChanged('id', false)) {
            if (self::findOne($this->id)) {
                throw new ErrorException('編號已經存在,請重新輸入');
            }
        }
        return true;
    }

    public function getSubType()
    {
        return $this->hasMany(TicketSubtype::className(), ['parent_id' => 'id'])->all();
    }
}
