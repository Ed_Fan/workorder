<?php

namespace app\models;

use app\repository\MemberRepository;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ticket;

/**
 * TicketSearch represents the model behind the search form about `app\models\Ticket`.
 */
class TicketSearch extends Ticket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'vote', 'type_id', 'platform_id'], 'integer'],
            [['content', 'create_at', 'process_at', 'accept_at', 'account', 'create_by', 'accept_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        $repository = new MemberRepository();
        if ($this->create_by) {
            if (!is_numeric($this->create_by)) {
                $model = $repository->findByUserName($this->create_by);
                $this->create_by = $model ? $model->id : 0;
            }
        }
        if ($this->accept_by) {
            if (!is_numeric($this->accept_by)) {
                $model = $repository->findByUserName($this->accept_by);
                $this->accept_by = $model ? $model->id : 0;
            }
        }
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ticket::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0 = 1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'          => $this->id,
            'create_by'   => $this->create_by,
            'create_at'   => $this->create_at,
            'process_at'  => $this->process_at,
            'accept_at'   => $this->accept_at,
            'accept_by'   => $this->accept_by,
            'status'      => $this->status,
            'vote'        => $this->vote,
            'type_id'     => $this->type_id,
            'platform_id' => $this->platform_id,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'account', $this->account]);

        return $dataProvider;
    }
}
