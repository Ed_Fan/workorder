<?php

namespace app\models;

use app\common\helpers\UserIdentityHelper;
use Yii;
use yii\base\ErrorException;

/**
 * This is the model class for table "ticket".
 *
 * @property integer $id
 * @property string $content
 * @property integer $create_by
 * @property string $create_at
 * @property string $process_at
 * @property string $accept_at
 * @property integer $accept_by
 * @property integer $status
 * @property integer $vote
 * @property integer $type_id
 * @property integer $platform_id
 * @property string $account
 */
class Ticket extends \yii\db\ActiveRecord
{
    CONST PROCESS_WAIT_FOR_ACCEPT = 0;
    CONST PROCESS_PROCESSING = 1;
    CONST PROCESS_WAIT_ASKER_RESPOND = 2;
    CONST PROCESS_WAIT_ANSWER_RESPOND = 3;
    CONST PROCESS_CLOSE_TICKET = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'type_id', 'platform_id'], 'required'],
            [['content'], 'string'],
            [['create_by', 'accept_by', 'status', 'vote', 'type_id', 'platform_id'], 'integer'],
            [['create_at', 'process_at', 'accept_at'], 'safe'],
            [['account'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => '編號',
            'content'     => '內容',
            'create_by'   => '創建者',
            'create_at'   => '創建時間',
            'process_at'  => '處理時間',
            'accept_at'   => '接手時間',
            'accept_by'   => '接手人',
            'status'      => '處理狀態',
            'vote'        => '評分',
            'type_id'     => '問題類型',
            'platform_id' => '平台',
            'account'     => '遊戲帳號',
        ];
    }

    public function getStatusLabel()
    {
        $labels = [
            self::PROCESS_CLOSE_TICKET        => '工单已關閉',
            self::PROCESS_PROCESSING          => '處理中',
            self::PROCESS_WAIT_ANSWER_RESPOND => '等待客服回應',
            self::PROCESS_WAIT_ASKER_RESPOND  => '等待用戶回應',
            self::PROCESS_WAIT_FOR_ACCEPT     => '等待客服接手',
        ];
        if ($this->status == self::PROCESS_WAIT_ASKER_RESPOND) {
            if (UserIdentityHelper::isAsker() && $this->create_by == Yii::$app->user->getId()) {
                $labels[self::PROCESS_WAIT_ASKER_RESPOND] = '<span style="color:red;">等您回复</span>';
            }
        }
        if ($this->status == self::PROCESS_WAIT_ANSWER_RESPOND || $this->status == self::PROCESS_PROCESSING) {
            if (UserIdentityHelper::isAnswer()) {
                if ($this->accept_by == Yii::$app->user->getId()) {
                    $labels[self::PROCESS_WAIT_ANSWER_RESPOND] = '<span style="color:red;">等您回复</span>';
                    $labels[self::PROCESS_PROCESSING] = '<span style="color:red;">等您回复</span>';
                }
            }
        }
        if ($this->status == self::PROCESS_WAIT_FOR_ACCEPT) {
            if (UserIdentityHelper::isAnswer()) {
                $labels[self::PROCESS_WAIT_FOR_ACCEPT] = '<span style="color:red;">等待客服接手</span>';
            }
        }
        return $labels;
    }

    /**
     * @inheritdoc
     * @return TicketQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TicketQuery(get_called_class());
    }

    /**
     * @return bool
     * @throws ErrorException
     */
    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->create_by = (int)Yii::$app->user->id;
            $this->create_at = date('Y-m-d H:i:s');
            if ($this->create_at && $this->create_by) {
                return true;
            }
            throw new ErrorException('系統錯誤請聯絡客服');
        }
        return true;
    }

    public function getTypeName()
    {
        return $this->hasOne(TicketSubtype::className(), ['id' => 'type_id'])->one();
    }


}
