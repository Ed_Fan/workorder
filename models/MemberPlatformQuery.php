<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MemberPlatform]].
 *
 * @see MemberPlatform
 */
class MemberPlatformQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return MemberPlatform[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MemberPlatform|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
