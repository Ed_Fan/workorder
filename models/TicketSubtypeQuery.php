<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TicketSubtype]].
 *
 * @see TicketSubtype
 */
class TicketSubtypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TicketSubtype[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TicketSubtype|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
