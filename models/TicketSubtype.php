<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ticket_subtype".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $template
 * @property integer $type_sorting
 */
class TicketSubtype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_subtype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'type_sorting'], 'integer'],
            [['name', 'parent_id'], 'required'],
            [['template'], 'string'],
            [['name'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'parent_id'    => '主類別',
            'name'         => '子類別',
            'template'     => '模板',
            'type_sorting' => '問題排序',
        ];
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            if (!$this->type_sorting) {
                $this->type_sorting = 99;
            }
        }
        return true;
    }

    /**
     * @inheritdoc
     * @return TicketSubtypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TicketSubtypeQuery(get_called_class());
    }

    public function getParentType()
    {
        return $this->hasOne(TicketType::className(), ['id' => 'parent_id'])->one();

    }
}
