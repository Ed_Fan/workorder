<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\base\InvalidCallException;

/**
 * This is the model class for table "member".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property integer $status
 * @property integer $type
 * @property string $chat_valid
 *
 * @property string $Password
 */
class Member extends ActiveRecord implements IdentityInterface
{
    public $Password;
    public $userIdentity;

    CONST TYPE_ASKER = 0;
    CONST TYPE_ANSWER = 1;


    public function getStateLabels()
    {
        return [
            self::TYPE_ASKER  => '用戶',
            self::TYPE_ANSWER => '客服',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            if (strlen($this->Password) < 8) {
                $this->addError('Password', '密码必须最少8位');
                return FALSE;
            }
            $this->setPassword($this->Password);
        } else {
            if ($this->Password != '' && strlen($this->Password) < 8) {
                $this->addError('Password', '密码必须最少8位');

                return FALSE;
            } elseif (strlen($this->Password) >= 8) {
                $this->setPassword($this->Password);
            }
        }

        return TRUE;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['status', 'type'], 'integer'],
            [['username'], 'string', 'max' => 30],
            [['salt', 'chat_valid'], 'string', 'max' => 40],
            [['password'], 'string', 'max' => 60],
            [['Password'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'username'   => '用戶名',
            'password'   => 'Password',
            'salt'       => 'Salt',
            'status'     => '狀態',
            'type'       => '帳號類型',
            'chat_valid' => 'Chat Valid',
        ];
    }

    /**
     * @inheritdoc
     * @return MemberQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MemberQuery(get_called_class());
    }

    /**
     * @param int|string $id
     * @return Member|array|null*
     */
    public static function findIdentity($id)
    {
        return static::find()->where(['id' => $id])->one();
    }

    public static function findIdentityByAccessToken($token, $type = NULL)
    {
        throw new InvalidCallException('未支持');
    }

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->salt;
    }

    public function validateAuthKey($authKey)
    {
        return $authKey === $this->salt;
    }

    public function userIdentity()
    {
        $type = $this->type;
        $this->userIdentity = $this->getStateLabels()[$type];
        if ($this->userIdentity === '客服') {
            if (in_array($this->id, \Yii::$app->params['allowAdmin'])) {
                $this->userIdentity = 'super';
            }
        }
        \Yii::$app->session->set('userIdentity', $this->userIdentity);
    }
}
