<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_platform".
 *
 * @property integer $uid
 * @property integer $pid
 * @property integer $id
 */
class MemberPlatform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member_platform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'pid'], 'required'],
            [['uid', 'pid'], 'integer'],
            [['uid', 'pid'], 'unique', 'targetAttribute' => ['uid', 'pid'], 'message' => 'The combination of Uid and Pid has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'pid' => 'Pid',
            'id' => 'ID',
        ];
    }

    /**
     * @inheritdoc
     * @return MemberPlatformQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MemberPlatformQuery(get_called_class());
    }
}
