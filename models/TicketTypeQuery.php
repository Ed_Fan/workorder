<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TicketType]].
 *
 * @see TicketType
 */
class TicketTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TicketType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TicketType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
