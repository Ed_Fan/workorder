<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ticket_logs".
 *
 * @property integer $id
 * @property integer $create_by
 * @property string $create_at
 * @property string $reason
 * @property integer $ticket_id
 */
class TicketLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_by', 'create_at', 'reason', 'ticket_id'], 'required'],
            [['create_by', 'ticket_id'], 'integer'],
            [['create_at'], 'safe'],
            [['reason'], 'string', 'max' => 600],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_by' => 'Create By',
            'create_at' => 'Create At',
            'reason' => 'Reason',
            'ticket_id' => 'Ticket ID',
        ];
    }

    /**
     * @inheritdoc
     * @return TicketLogsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TicketLogsQuery(get_called_class());
    }
}
