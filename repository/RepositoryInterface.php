<?php
/**
 * Date: 17/3/27 04:10
 */

namespace app\repository;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

interface RepositoryInterface {


    /**
     * @return ActiveQuery
     */
    public function find();
}