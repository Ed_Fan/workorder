<?php
/**
 * Date: 2017/6/20
 */

namespace app\repository;

use app\models\Member;
use yii\db\ActiveRecord;

class MemberRepository extends BaseRepository
{
    public $_model;

    public function __construct($model = null)
    {
        $_model = $model ? $model : new Member();
        parent::__construct($_model);
    }

    public function findByUserName($userName)
    {
        $model = $this->find()->where(['username' => $userName])->one();
        return $model ? $model : null;
    }

    public static function getUserName($id)
    {
        $model = Member::findOne($id);
        return $model ? $model->username : null;
    }

}