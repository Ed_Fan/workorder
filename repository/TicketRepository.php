<?php
/**
 * Date: 2017/6/23
 * Time: 下午 05:56
 */

namespace app\repository;

use app\models\Ticket;
use yii\db\ActiveRecord;

class TicketRepository extends BaseRepository
{
    public $_model;

    public function __construct($model = null)
    {
        $_model = $model ? $model : new Ticket();
        parent::__construct($_model);
    }

    public function getColumnToArray($column)
    {
//        return $this->find()->select($column);
    }

    public function openTicket()
    {
        if ($this->_model->status) {
            $this->_model->status = 1;
            if ($this->create()) {
                return true;
            }
        }
        return false;
    }

    public function closeTicket()
    {
        if ($this->_model->status) {
            $this->_model->status = 4;
            if ($this->create()) {
                return true;
            }
        }
        return false;
    }


}