<?php
/** * Date: 2017/6/23
 * Time: 下午 06:57
 */

namespace app\repository;

use app\models\TicketType;
use yii\db\ActiveRecord;

class TicketTypeRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(new TicketType());
    }

    public function getColumn($column = [])
    {
        return $this->find()->select($column)->all();
    }
}