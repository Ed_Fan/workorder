<?php
/** * Date: 2017/6/23
 * Time: 下午 06:49
 */

namespace app\repository;

use app\models\TicketSubtype;
use yii\db\ActiveRecord;

class TicketSubtypeRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(new TicketSubtype());
    }

    public function getColumn($column = [])
    {
        return $this->find()->select($column)->all();
    }
}