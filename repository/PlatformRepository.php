<?php
/**
 * Date: 2017/6/23
 * Time: 下午 06:07
 */

namespace app\repository;

use app\models\Platform;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class PlatformRepository extends BaseRepository
{
    public $_model;

    public function __construct($model = null)
    {
        $_model = $model ? $model : new Platform();
        parent::__construct($_model);
    }

    public function getColumn($column = [])
    {
        return $this->find()->select($column)->all();
    }
}