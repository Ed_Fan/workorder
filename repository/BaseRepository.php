<?php
/**
 * Date: 17/3/26 20:55
 */

namespace app\repository;


use yii\db\ActiveRecord;
use yii\web\HttpException;

class BaseRepository implements RepositoryInterface
{
    /**
     * @var $_model ActiveRecord
     */
    protected $_model = NULL;

    public function __construct(ActiveRecord $activeRecord)
    {
        $this->_model = $activeRecord;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function find()
    {
        return $this->_model->find();
    }

    public function create()
    {
        if ($this->_model->save()) {
            return TRUE;
        }
        throw new \ErrorException(array_pop($this->_model->getFirstErrors()));
    }
}