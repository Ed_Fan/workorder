<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TicketSubtype */

$this->title = Yii::t('app', '建立子分類');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '問題分類管理'), 'url' => \yii\helpers\Url::to(['../ticket-type/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '子分類'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-subtype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
