<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TicketSubtypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '子分類管理');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '問題分類管理'), 'url' => \yii\helpers\Url::to(['../ticket-type/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-subtype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', '建立分類'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'parent_id',
                'format'    => 'raw',
                'value'     => function ($data) {
                    $model = $data->getParentType();
                    return $model ? $model->name : '';
                },
            ],
            'name',
            //'template:ntext',
            'type_sorting',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update},{delete}',
            ],
        ],
    ]); ?>
</div>
