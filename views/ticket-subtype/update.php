<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TicketSubtype */

$this->title = Yii::t('app', '更新 {modelClass}: ', [
    'modelClass' => '子分類',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '問題分類管理'), 'url' => \yii\helpers\Url::to(['../ticket-type/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '子分類'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', '更新');
?>
<div class="ticket-subtype-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
