<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TicketSubtype */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-subtype-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-sm-6">
        <div class="col-md-8">
            <?= $form->field($model, 'parent_id')->dropDownList(\app\services\TicketTypeService::getTypeList(), ['prompt' => '請選擇']) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type_sorting')->textInput()->label('問題排序(小到大)') ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'template')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
