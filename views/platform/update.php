<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Platform */

$this->title = Yii::t('app', '更新 {modelClass}: ', [
    'modelClass' => '平台',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '平台管理'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', '更新');
?>
<div class="platform-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
