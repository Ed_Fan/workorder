<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->title = '工單詳情 #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '我的工單'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$statusChange = <<<js
    $('.change-status').on('click',function() {
      var operate = $(this).val();
      $.ajax({
          url:operate+'?id={$model->id}',
          type:'POST',
          dataType:'json',
          success:function(data) {
                if (data===true){
                    layer.msg('修改完成',{time:3000});
                }
                location.reload();    
          }
      });
    });
js;

$this->registerJs($statusChange);
?>
<div class="ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if ($model->status === 4) {
            $value = 'open-ticket';
            $operate = '打開工單';
        } else {
            $value = 'close-ticket';
            $operate = '關閉工單';
        }
        echo Html::button(Yii::t('app', $operate), ['class' => 'btn btn-primary change-status', 'value' => $value]) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>
    <div class="col-md-9">

        <?= DetailView::widget([
            'model'      => $model,
            'attributes' => [
                'id',
                'content:ntext',
                'create_by',
                'create_at',
                'process_at',
                'accept_at',
                'accept_by',
                'status',
                'vote',
                'type_id',
                'platform_id',
                'account',
            ],
        ]) ?>
    </div>
</div>
