<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TicketSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-search col-md-12" >

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',
    ]); ?>

    <?= $this->render('_timeSearch', [
        'model' => $model,
    ]) ?>

    <div class="col-md-4">
        <?= $form->field($model, 'type_id')->dropDownList(\app\services\TicketSubtypeService::getParentNameList(),['prompt'=>'請選擇']) ?>

        <?php if (\app\common\helpers\UserIdentityHelper::isAsker()) {
            $model->create_by = Yii::$app->user->identity->username;
            $readOnly = true;
        }
        echo $form->field($model, 'create_by')->textInput(['readonly' => isset($readOnly) ? $readOnly : false]) ?>

        <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'content') ?>
    </div>
    <div class="col-md-4">

        <?= $form->field($model, 'platform_id')->dropDownList(\app\services\PlatformService::getPlatformList(),['prompt'=>'請選擇']) ?>

        <?= $form->field($model, 'accept_by') ?>

        <?= $form->field($model, 'account') ?>

    </div>

    <?php // $form->field($model, 'create_at') ?>

    <?php // $form->field($model, 'process_at') ?>

    <?php // echo $form->field($model, 'accept_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'vote') ?>


    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
