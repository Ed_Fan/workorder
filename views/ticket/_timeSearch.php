<?php
/**
 * Date: 2017/5/3
 * Time: 下午 02:14
 */

use yii\helpers\Html;

$layDate = <<<hhh
    var start = {
        elem: '#DateStart',
        format: 'YYYY/MM/DD',
       // min: laydate.now(), //设定最小日期为当前日期
        max: laydate.now(), //最大日期
        istime: true,
        istoday: false,
        choose: function(datas){
             end.min = datas; //开始日选好后，重置结束日的最小日期
             end.start = datas //将结束日的初始值设定为开始日
        }
    };
    var end = {
        elem: '#DateEnd',
        format: 'YYYY/MM/DD',
        max: laydate.now(),
        istime: true,
        istoday: false,
        choose: function(datas){
            start.max = datas; //结束日选好后，重置开始日的最大日期
        }
    };
    laydate(start);
    laydate(end);
hhh;
$this->registerJs($layDate);
$this->registerJsFile('/laydate/laydate.js', [
    'depends' => 'yii\web\YiiAsset',
]);
?>
<div class="form-group field-ticketsearch-create_at">
    <h4 style="color: red">*時間搜尋可以只選擇起始或是截止日期</h4>
    <div class="col-md-4">
        <?= Html::label('創建於', null, ['class' => 'control-label col-sm-3']) ?>
        <div class="col-md-7">
            <?= Html::textInput('DateStart', '', ['class' => 'form-control', 'id' => 'DateStart','placeholder' => '起始日期']) ?>
        </div>

    </div>
    <div class="col-md-2">
        <?= Html::textInput('DateEnd', '', ['class' => 'form-control', 'id' => 'DateEnd','placeholder' => '截止日期']) ?>
    </div>

</div>


