<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\repository\MemberRepository;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '我的工單');
$this->params['breadcrumbs'][] = $this->title;
$url = \yii\helpers\Url::to(['ticket']);

$js = <<<js
    $('.search-button').click(function(){
	$('.ticket-search').toggle();
	return false;
});
js;
$this->registerJs($js);

?>
<div class="ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--    --><?php //echo Html::button('搜尋工單', ['class' => 'search-button btn btn-primary']) ?>

    <?php echo $this->render('_search', [
        'model' => $searchModel,
    ]); ?>
    <div style="margin-top: 0.5cm ;margin-bottom: 0.5cm">
        <?= Html::a('等我處理', ['index', 'TicketSearch[status]' => 1], ['style' => 'margin-right:0.5cm ;color:red;text-decoration:underline']) ?>
        <?= Html::a('等待接手', ['index', 'TicketSearch[status]' => 2], ['style' => 'margin-right:0.5cm ; color:red;text-decoration:underline']) ?>
        <?= Html::a('顯示全部', ['index'], ['style' => 'margin-right:0.5cm;text-decoration:underline']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'id',
            [
                'attribute' => 'platform_id',
                'value'     => function ($data) {
                    $platform = \app\services\PlatformService::getPlatformList()[$data->platform_id];
                    return $platform ? $platform : '';
                },
            ],
            'account',
            [
                'attribute' => 'type_id',
                'value'     => function ($data) {
                    return $data->getTypeName()->name;
                },
            ],
            'content:ntext',
            [
                'attribute' => 'content',
                'format'    => 'raw',
                'value'     => function ($data) {
                    $platform = \app\services\PlatformService::getPlatformList()[$data->platform_id];
                    return Html::a('[' . $platform . ']' . $data->getTypeName()->name, ['view', 'id' => $data->id]);
                },
            ],
            [
                'attribute' => 'create_by',
                'value'     => function ($data) {
                    $name = MemberRepository::getUserName($data->create_by);
                    return $name ? $name : '';
                },
            ],
            'create_at',
            'process_at',
            'accept_at',
            [
                'attribute' => 'accept_by',
                'value'     => function ($data) {
                    $name = MemberRepository::getUserName($data->accept_by);
                    return $name ? $name : '';
                },
            ],
            [
                'attribute' => 'status',
                'format'    => 'raw',
                'value'     => function ($data) {
                    return $data->getStatusLabel() ? $data->getStatusLabel()[$data->status] : '';
                },
            ],
            'vote',
            ['class'    => 'yii\grid\ActionColumn',
             'template' => '{view}',
            ],
        ],
    ]); ?>
</div>
