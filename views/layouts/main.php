<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$this->title = Yii::$app->name;
$this->registerJsFile('/layer/layer.js', [
    'depends' => 'yii\web\YiiAsset',
]);
$this->registerJsFile('/layer/extend/layer.ext.js', [
    'depends' => 'yii\web\YiiAsset',
]);
//$this->registerJsFile('/js/jquery.timer.js', [
//    'depends' => 'yii\web\YiiAsset',
//]);

$js = <<<HTML
var layerTipIndex = false;
$('.tip').hover(function(){
    layerTipIndex = layer.tips($(this).attr('title'),$(this), {
        tips: [1, '#0FA6D8'] //还可配置颜色
    });
},function(){
    layer.close(layerTipIndex);
});


HTML;
$this->registerJs($js, \yii\web\View::POS_END);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => '所有功能', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems = array_merge($menuItems, app\services\MenuItemService::getMenuItems());
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items'   => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
<script type="text/javascript">
    <?php if( Yii::$app->session->hasFlash('success')):?>
    layer.msg('<?php echo Yii::$app->session->getFlash('success');?>');
    <?php endif;?>
    <?php if( Yii::$app->session->hasFlash('error')):?>
    layer.alert('<?php echo Yii::$app->session->getFlash('error');?>', {icon: 2});
    <?php endif;?>
    <?php if( Yii::$app->session->hasFlash('warning')):?>
    layer.alert('<?php echo Yii::$app->session->getFlash('warning');?>', {icon: 0});
    <?php endif;?>
    <?php if( Yii::$app->session->hasFlash('notice')):?>
    layer.alert('<?php echo Yii::$app->session->getFlash('notice');?>', {icon: 6});
    <?php endif;?>

</script>
</body>
</html>
<?php $this->endPage() ?>
