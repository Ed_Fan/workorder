<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MemberSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-search col-md-12">
    <div class="col-md-3">


        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <!--    --><?php //$form->field($model, 'id') ?>

        <?= $form->field($model, 'username') ?>

        <!--    --><?php //$form->field($model, 'password') ?>
        <!---->
        <!--    --><?php //$form->field($model, 'salt') ?>

        <!--    --><?php //$form->field($model, 'status') ?>

        <?php // echo $form->field($model, 'type') ?>

        <?php // echo $form->field($model, 'chat_valid') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
