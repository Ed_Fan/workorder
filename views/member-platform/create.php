<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MemberPlatform */

$this->title = Yii::t('app', 'Create Member Platform');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Member Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-platform-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
