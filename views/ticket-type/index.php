<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TicketTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '問題分類管理');
$this->params['breadcrumbs'][] = $this->title;
$url = \yii\helpers\Url::to(['ticket-type']);
?>
<div class="ticket-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', '建立問題分類'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'name',
//            'template:ntext',
            [
                'label'  => '子問題分類',
                'format' => 'raw',
                'value'  => function ($data) {
                    return Html::a('子分類', [\yii\helpers\Url::to(['../ticket-subtype']),
                        'TicketSubtypeSearch[parent_id]' => $data->id]);
                },
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>
</div>
