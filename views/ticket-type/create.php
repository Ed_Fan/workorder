<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TicketType */

$this->title = Yii::t('app', '建立問題分類');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '問題分類管理'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
